package JUDPServer;

import java.io.IOException;
import java.net.*;

@SuppressWarnings("unused")
public abstract class Server extends Thread {

	/**
	 * the server's socket used by this server
	 */
	private DatagramSocket socket;

	/**
	 * Construct a server and listen a specific port.
	 * @param port  the port to listening
	 * @throws SocketException
	 */
	public Server(int port) throws SocketException {
		this.socket = new DatagramSocket(port);
	}

	/**
	 * Construct a server and listen a specific port.
	 * @param port      the port to listening
	 * @param ipAddress the port to listening
	 * @throws SocketException
	 */
	public Server(int port, InetAddress ipAddress) throws SocketException {
		this.socket = new DatagramSocket(port, ipAddress);
	}

	public Server(int port, String hostname) throws UnknownHostException, IndexOutOfBoundsException, SocketException {
		this.socket = new DatagramSocket(port, InetAddress.getAllByName(hostname)[0]);
	}

	/**
	 * Function used when starting the server
	 */
	@Override
	public void run() {
		int datagramPacketLength = getNewInstanceOfService().getDatagramPacketLength();

		while(!this.isInterrupted()) {

			DatagramPacket datagramPacket = new DatagramPacket(new byte[datagramPacketLength], datagramPacketLength);

			try {
				this.socket.receive(datagramPacket);
			}
			catch (IOException e) {
				synchronized (System.out) {
					System.err.println("Server's error - I/O Error");
					e.printStackTrace();
				}
				continue;
			}

			Service service = this.getNewInstanceOfService();
			service.setDatagramPacket(datagramPacket);
			service.setDatagramSocket(this.socket);
			service.start();
		}
	}

	/**
	 * @see ServerSocket#getInetAddress()
	 */
	@SuppressWarnings("SpellCheckingInspection")
	public InetAddress getInetAddress() {
		return this.socket.getLocalAddress();
	}

	/**
	 * @see ServerSocket#getLocalPort()
	 */
	public int getPort() {
		return this.socket.getLocalPort();
	}

	/**
	 * create a new instance of the service.
	 * @return the instance of the service created.
	 */
	protected abstract Service getNewInstanceOfService();

	@Override
	protected void finalize() throws Throwable {
		super.finalize();

		if (!this.socket.isClosed())
			this.socket.close();
	}
}
