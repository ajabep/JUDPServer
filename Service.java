package JUDPServer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public abstract class Service extends Thread {

	protected DatagramPacket datagramPacket;
	protected DatagramSocket datagramSocket;

	@Override
	public abstract void run();

	public void setDatagramPacket(DatagramPacket datagramPacket) {
		this.datagramPacket = datagramPacket;
	}

	public void setDatagramSocket(DatagramSocket datagramSocket) {
		this.datagramSocket = datagramSocket;
	}

	public abstract int getDatagramPacketLength();
}
