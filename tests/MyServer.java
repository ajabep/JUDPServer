package JUDPServer.tests;

import JUDPServer.Server;
import JUDPServer.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;

public class MyServer extends Server {
	public MyServer(int port) throws SocketException {
		super(port);
	}

	public MyServer(int port, String hostname) throws IOException, SecurityException {
		super(port, hostname);
	}

	public MyServer(int port, InetAddress ipAddress) throws SocketException {
		super(port, ipAddress);
	}

	@Override
	protected Service getNewInstanceOfService() {
		return new MyService();
	}
}
