package JUDPServer.tests;

import JUDPServer.Server;
import org.junit.Test;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import static org.junit.Assert.assertEquals;

public class ServerTest {
	@Test
	public void test1_LaunchServer() throws Exception {
		int port = 9091;

		Server myServer = new MyServer(++port);
		myServer.start();

		assertEquals("The server port must be the port we wanted.", port, myServer.getPort());

		myServer.interrupt();


		String hostname = "localhost";
		myServer = new MyServer(++port, hostname);
		myServer.start();

		assertEquals("The server hostname must be the hostname we wanted.", hostname, myServer.getInetAddress().getHostName());
		assertEquals("The server port must be the port we wanted.", port, myServer.getPort());

		myServer.interrupt();


		String address = "127.0.0.1";
		myServer = new MyServer(++port, address);
		myServer.start();


		assertEquals("The server address must be the address we wanted.", address, myServer.getInetAddress().getHostAddress());
		assertEquals("The server port must be the port we wanted.", port, myServer.getPort());

		myServer.interrupt();
	}

	@Test(timeout = 1000L) // = timeout at 1 sec
	public void test2_IO() throws Exception {
		Server myServer = new MyServer(9090);
		myServer.start();

		// UDP Socket
		DatagramSocket datagramSocket = new DatagramSocket();

		String strToPrint = "test I/O of JTCPServer";


		// Forge UDP packet
		DatagramPacket datagramPacket = new DatagramPacket(
				strToPrint.getBytes(),
				strToPrint.getBytes().length,
				myServer.getInetAddress(),
				myServer.getPort()
		);

		// send package
		datagramSocket.send(datagramPacket);


		// receive packet (it override datagramPacket)
		datagramSocket.receive(datagramPacket);


		assertEquals("The server must write what this thread had written", strToPrint, new String(datagramPacket.getData()));

		datagramSocket.close();
		myServer.interrupt();
	}
}
