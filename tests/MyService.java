package JUDPServer.tests;

import JUDPServer.Service;

import java.io.IOException;

public class MyService extends Service {
	@Override
	public void run() {
		try {
			Thread.sleep(1L);

			datagramSocket.send(this.datagramPacket);
		}
		catch (InterruptedException | IOException ignore) {}
	}

	@Override
	public int getDatagramPacketLength() {
		return 65535;
	}
}
